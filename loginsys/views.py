# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.contrib.auth.forms import AuthenticationForm

# Create your views here.


def login(request):
    args = {}
    args.update(csrf(request))
    args['form'] = AuthenticationForm()
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/viewer')
        else:
            args['login_error'] = "Имя пользователя или пароль не найдены"
            return render_to_response('login.html', args)

    else:
        return render_to_response('login.html', args)


def logout(request):
    auth.logout(request)
    response = redirect('/auth/login')
    response.delete_cookie('sessionid')
    response.delete_cookie('csrftoken')
    return response