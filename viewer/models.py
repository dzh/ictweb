# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class ObjectUser(models.Model):
    webuser = models.ForeignKey(User)
    objectcode = models.IntegerField()
    objectname = models.CharField(max_length=255)
