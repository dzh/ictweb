from django.test import TestCase
from viewer.ict import Ict


class IctTest(TestCase):
    def setUp(self):
        self.ict = Ict()

    def test_get_objects(self):
        objects_from_db = self.ict.getObjects()

        self.assertEquals(len(objects_from_db) > 0, True)

    def _test_make_currdata(self):
        self.assertEquals(self.ict.makeCurrData(100), True)