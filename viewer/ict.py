# -*- coding: utf-8 -*-
"""
Модуль реализует объект доступа к базе данных gas_eqip.mdb через pyodbc
по ODBC DSN альясу программы учета газа Indel Control Terminal
"""

try:
    import pypyodbc as pyodbc
except ImportError:
    pass

import datetime

DSN_ALIAS = 'GASEQUIPMENT2'
PASSWORD = '121'

REQUEST_TYPE_CURR = 1


class Ict():
    """
    Класс доступа к объектам базы
    """
    def __init__(self):
        """
        Конструктор объекта доступа к данным. Создаем соединение с базой
        """
        self.dblink = pyodbc.connect('DSN=' + DSN_ALIAS +
                                        ';PWD=' + PASSWORD + ';')
        self.cursor = self.dblink.cursor()

    def __del__(self):
        """
        Деструктор объекта доступа к данным. Чистим соединение с базой
        """
        self.cursor.close()
        del self.cursor
        self.dblink.close()

    def getObjectsEx(self):
        """ Получаем список объектов из базы (с установкой соединения) """
        with pyodbc.connect('DSN=' + DSN_ALIAS +
                                        ';PWD=' + PASSWORD + ';') as dblink:
            with dblink.cursor() as cursor:
                query = 'select * from objects'
                cursor.execute(query)
                fields = [name[0] for name in cursor.description]
                rows = cursor.fetchall()
                #  list of dict
                return [dict(zip(fields, row)) for row in rows]

    def getCurrByIdEx(self, object_id):
        """ Получаем текущие данные из базы по объекту с object_id
            (с установкой соединения)
        """
        with pyodbc.connect('DSN=' + DSN_ALIAS +
                                        ';PWD=' + PASSWORD + ';') as dblink:
            with dblink.cursor() as cursor:
                query = 'select * from CurrentData where ObjectCode=?'
                cursor.execute(query, (object_id,))
                fields = [name[0] for name in cursor.description]
                row = cursor.fetchone()
                if row:
                    return dict(zip(fields, row))
                else:
                    return None

    def getObjects(self):
        """ Получаем список объектов из базы (используем соединение
        из конструктора)
        """
        query = 'select * from objects'
        self.cursor.execute(query)
        fields = [name[0] for name in self.cursor.description]
        rows = self.cursor.fetchall()
        #  list of dict
        return [dict(zip(fields, row)) for row in rows]

    def getCurrById(self, object_id):
        """ Получаем текущие данные из базы по объекту с object_id
            (используем соединение из конструктора)
        """
        query = 'select * from CurrentData where ObjectCode=?'
        self.cursor.execute(query, (object_id,))
        fields = [name[0] for name in self.cursor.description]
        row = self.cursor.fetchone()
        if row:
            return dict(zip(fields, row))
        else:
            return None

    def makeCurrData(self, object_id):
        """ Добавляем опрос текущих данных по объекту с object_id
            (используем соединение из конструктора)
        """
        query = 'insert into schedule(StartDateTime, ObjectId, RequestType, Status) values (?, ?, ?, ?)'
        self.cursor.execute(query,
            (datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S"),
            object_id,
            REQUEST_TYPE_CURR,
            1))
        return self.dblink.commit()