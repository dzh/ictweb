from django.contrib import admin
from .models import ObjectUser
from django import forms
import ict


class ObjectUserForm(forms.ModelForm):
    ict = ict.Ict()
    frmobjects = []
    for i in ict.getObjects():
        frmobjects.append((i['objectcode'], i[u'name']))

    objects = forms.ChoiceField(choices=frmobjects)

    class Meta:
        model = ObjectUser
        exclude = ['objectcode', 'objectname']


class ObjectUserAdmin(admin.ModelAdmin):
    form = ObjectUserForm
    list_display = ('webuser', 'objectname')
    list_filter = ['webuser']

    def save_model(self, request, obj, form, change):
        obj.objectcode = form.cleaned_data['objects']
        obj.objectname = dict(form.fields['objects'].choices)[int(obj.objectcode)]
        obj.save()

# Register your models here.
admin.site.register(ObjectUser, ObjectUserAdmin)