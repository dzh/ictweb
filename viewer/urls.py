# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from viewer import views

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^getcurr/(?P<object_id>[0-9]+)/$',
        views.get_current, name="get_current"),
    url(r'^makecurrent/(?P<object_id>[0-9]+)/$',
        views.make_current, name="make_current"),
)

