# -* coding: utf-8 -*-

from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

# Create your views here.

from ict import Ict
import formatdata as fd
from .models import ObjectUser


@login_required(login_url='/auth/login')
def index(request):
    ictobj = Ict()
    objects = ictobj.getObjects()
    # отфильтровать объекты пользователя по request.user.id
    objectuser = ObjectUser.objects.all(). \
        filter(webuser__id=request.user.id). \
        values_list('objectcode', flat=True)
    filterobjects = []
    for i in objects:
        if i['objectcode'] in objectuser:
            filterobjects.append(i)
    return  render(request, 'viewer/index.html',
        {'objects': filterobjects,
        'username': request.user.username})


@login_required(login_url='/auth/login')
def get_current(request, object_id):
    ictobj = Ict()
    data = ictobj.getCurrById(object_id)
    frmdata = {}
    if data:
        # fix datetime
        data['time'] = data['time'].isoformat()

        # пропуск неиспользуемых, перевод имен полей
        for key in data:
            if key not in fd.FIELDS_IGNOR and key in fd.FIELDS_NAME:
                frmdata[fd.FIELDS_NAME[key]] = data[key]
    return render_to_response("viewer/currdata.html", {'currdata': frmdata})


@login_required(login_url='/auth/login')
def make_current(request, object_id):
    ictobj = Ict()
    result = ictobj.makeCurrData(object_id)
    return  HttpResponse(result)